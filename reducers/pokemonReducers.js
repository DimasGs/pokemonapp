import { FETCH_SUCCEEDED } from "../actions/actionTypes";

const initialState = {
  pokemon: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUCCEEDED:
      console.log(
        "action.type pokemon reducer",
        action.type,
        action.receivedPokemon
      );
      return {
        pokemon: action.receivedPokemon,
      };
    default:
      return state;
  }
};
