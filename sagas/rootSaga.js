import { call, all } from "redux-saga/effects";
import { fork } from "redux-saga/effects";
import { watchFetchPokemon, watchFetchItem } from "./pokemonSagas";

// export default function* rootSaga() {
//   yield [fork(watchFetchPokemon), fork(watchFetchItem)];
// }
export default function* rootSaga() {
  // yield call(watchFetchPokemon, watchFetchItem);
  // yield call(fetchPokemon);
  yield all([
    call(watchFetchPokemon), // task1
    call(watchFetchItem), // task2,
  ]);
}
