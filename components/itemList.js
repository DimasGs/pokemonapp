import React, { useState, useEffect, Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { fetchItemAction } from "../actions";
const itemList = (props) => {
  const item = useSelector((item) => item.item.item);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  async function fetchItem() {
    await dispatch(fetchItemAction());
  }

  useEffect(() => {
    setIsLoading(true);
    console.log("useEffect Called");
    fetchItem();
    setIsLoading(false);
  }, []);
  if (isLoading) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator size="large" color="#000" />;
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={item}
        keyExtractor={(item) => item.name}
        renderItem={({ item, index }) => (
          <Text
            style={{
              padding: 10,
              fontWeight: "bold",
              fontSize: 17,
              color: "white",
              backgroundColor:
                index % 2 === 0 ? "dodgerblue" : "mediumseagreen",
            }}
          >
            {console.log("item List", item)}
            {`${item.name}`}
          </Text>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

itemList.navigationOptions = (navData) => {
  return {
    headerTitle: "List Item",
  };
};

export default itemList;
