import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
  StyleSheet,
  Button,
  View,
  SafeAreaView,
  Text,
  Alert,
  FlatList,
} from "react-native";

const Separator = () => <View style={styles.separator} />;
export default class homeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { pokemon: [] };
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.button}>
          <Button
            title="List Pokemon"
            onPress={() => {
              this.props.navigation.navigate("PokemonList");
            }}
          />
        </View>
        <Separator />
        <View style={styles.button}>
          <Button
            title="List Item"
            onPress={() => this.props.navigation.navigate("itemList")}
          />
        </View>
        <Separator />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //   justifyContent: 'center',
    //   marginHorizontal: 16,
    backgroundColor: "#fff",
  },
  button: {
    margin: 10,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

homeScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Pokemon App",
  };
};
