import {
  FETCH_POKEMON,
  FETCH_ITEM,
  FETCH_SUCCEEDED,
  FETCH_FAILED,
} from "./actionTypes";

export const fetchPokemonAction = (sort) => {
  return {
    type: FETCH_POKEMON,
    sort,
  };
};

export const fetchItemAction = (param) => {
  console.log("fetchItemAction");
  return {
    type: FETCH_ITEM,
    param,
  };
};

export const fetchSuccessAction = (data) => {
  return {
    type: FETCH_SUCCEEDED,
    data,
  };
};
export const fetchFailedAction = (failed) => {
  return {
    type: FETCH_FAILED,
    failed,
  };
};
