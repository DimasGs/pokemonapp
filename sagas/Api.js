const urlGetPokemon = "https://pokeapi.co/api/v2/pokemon/";
const urlGetItem = "https://pokeapi.co/api/v2/item/";

async function getPokemonFromApi() {
  console.log("CALLED BY CLICK");
  try {
    const response = await fetch(urlGetPokemon);
    const json = await response.json();
    const arr = json.results;
    return arr;
  } catch (e) {
    throw new Error(`fetching list of pokemons went wrong`);
  }
}
async function getItemFromApi() {
  console.log("getItemFromApi Called");
  try {
    const response = await fetch(urlGetItem);
    const json = await response.json();
    const arr = json.results;
    return arr;
  } catch (e) {
    throw new Error(`fetching list of items went wrong`);
  }
}
export const Api = {
  getPokemonFromApi,
  getItemFromApi,
};
