import { connect } from "react-redux";
import pokemonList from "../components/pokemonList";
import homeScreen from "../components/homeScreen";

import {
  fetchPokemonAction,
  fetchItemAction,
  fetchSuccessAction,
  fetchFailedAction,
} from "../actions";
const mapStateToProps = (state) => {
  return {
    pokemon: state.pokemonReducers,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPokemon: () => {
      dispatch(fetchPokemonAction());
    },
    onFetchitem: () => {
      dispatch(fetchItemAction());
    },
  };
};

// const PokemonContainer = connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(pokemonList);
// export default PokemonContainer;

export { mapDispatchToProps, mapStateToProps };
