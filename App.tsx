import React, { useState, Component } from "react";
import { AppRegistry } from "react-native";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import AppLoading from "expo-app-loading";
import * as Font from "expo-font";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import createSagaMiddleware from "redux-saga";
// import PokemonContainer from "./containers/pokemonContainer";
import rootSaga from "./sagas/rootSaga";
import homeScreen from "./components/homeScreen";
import PokemonList from "./components/pokemonList";
import pokemonDetail from "./components/pokemonDetail";
import itemList from "./components/itemList";
import itemDetail from "./components/itemDetail";
import pokemonReducers from "./reducers/pokemonReducers";
import itemReducer from "./reducers/itemReducer";

const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers({
  pokemon: pokemonReducers,
  item: itemReducer,
});
let store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

const AppNavigator = createStackNavigator(
  {
    homeScreen: homeScreen,
    PokemonList: PokemonList,
    pokemonDetail: pokemonDetail,
    itemList: itemList,
    itemDetail: itemDetail,
  },
  {
    initialRouteName: "homeScreen",
  }
);
const App = createAppContainer(AppNavigator);
sagaMiddleware.run(rootSaga);

export default () => {
  const [appLoaded, setAppLoaded] = useState(false);

  const loadAppAssets = () => {
    return Font.loadAsync({
      Pokemon: require("./assets/fonts/Pokemon.ttf"),
    });
  };

  return appLoaded ? (
    <Provider store={store}>
      <App />
      {/* <PokemonContainer /> */}
    </Provider>
  ) : (
    <AppLoading
      startAsync={loadAppAssets}
      onFinish={() => {
        setAppLoaded(true);
      }}
      onError={console.error}
    />
  );
};
