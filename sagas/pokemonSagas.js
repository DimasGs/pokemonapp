import {
  FETCH_POKEMON,
  FETCH_SUCCEEDED,
  FETCH_FAILED,
  FETCH_ITEM,
} from "../actions/actionTypes";
//Saga effects
import { put, takeLatest } from "redux-saga/effects";
import { Api } from "./Api";

function* fetchPokemon() {
  try {
    const receivedPokemon = yield Api.getPokemonFromApi();
    yield put({ type: FETCH_SUCCEEDED, receivedPokemon: receivedPokemon });
  } catch (error) {
    yield put({ type: FETCH_FAILED, error });
  }
}
export function* watchFetchPokemon() {
  yield takeLatest(FETCH_POKEMON, fetchPokemon);
}

function* fetchItem() {
  console.log("sagas called");
  try {
    const receivedItem = yield Api.getItemFromApi();
    yield put({ type: FETCH_ITEM, receivedItem: receivedItem });
  } catch (error) {
    // yield put({ type: FETCH_FAILED, error });
  }
}
export function* watchFetchItem() {
  yield takeLatest(FETCH_ITEM, fetchItem);
}
//export { fetchPokemon, fetchItem };
