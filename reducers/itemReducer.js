import { FETCH_SUCCEEDED, FETCH_ITEM } from "../actions/actionTypes";

const initialState = {
  item: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEM:
      console.log("action.type item reducer", action.type, action.receivedItem);
      return {
        item: action.receivedItem,
      };
    default:
      return state;
  }
};
