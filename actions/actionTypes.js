export const FETCH_POKEMON = "FETCH_POKEMON";
export const FETCH_ITEM = "FETCH_ITEM";

export const FETCH_SUCCEEDED = "FETCH_SUCCEEDED";
export const FETCH_FAILED = "FETCH_FAILED";
