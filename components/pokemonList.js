import React, { useState, useEffect, Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { fetchPokemonAction } from "../actions";
// export default class pokemonList extends Component {
const pokemonList = (props) => {
  const pokemon = useSelector((art) => art.pokemon.pokemon);
  const [isLoading, setIsLoading] = useState(false);
  // constructor(props) {
  //   super(props);
  //   this.state = { movieName: "", releaseYear: "" };
  // }
  const dispatch = useDispatch();
  async function fetchPokemon() {
    await dispatch(fetchPokemonAction());
  }

  useEffect(() => {
    setIsLoading(true);
    console.log("useEffect Called");
    fetchPokemon();
    setIsLoading(false);
  }, []);
  if (isLoading) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator size="large" color="#000" />;
      </View>
    );
  }
  // render() {
  return (
    <View style={styles.container}>
      <FlatList
        data={pokemon}
        keyExtractor={(item) => item.name}
        renderItem={({ item, index }) => (
          <Text
            style={{
              padding: 10,
              fontWeight: "bold",
              fontSize: 17,
              color: "white",
              backgroundColor:
                index % 2 === 0 ? "dodgerblue" : "mediumseagreen",
            }}
          >
            {`${item.name}`}
          </Text>
        )}
      />
    </View>
  );
};
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

pokemonList.navigationOptions = (navData) => {
  return {
    headerTitle: "List Pokemon",
  };
};

export default pokemonList;
